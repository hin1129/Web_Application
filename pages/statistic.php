<?php
	/*Connect to database*/
	require_once '../connection.php';
	if($db->connect_errno > 0){
		die('Unable to connect to database [' . $db->connect_error . ']');
	}

	//Page title
	$title = 'Statistical';

	require_once 'header.php';


	//
	//
	// task details section
	//
	//
	// show task details
	$get = $db->query("SELECT * FROM tasks WHERE username = '{$_COOKIE['user']}' ORDER BY time DESC;");

	$Mon = $Tue = $Wed = $Thu = $Fri = $Sat = $Sun = [];

	while ($res = $get->fetch_assoc()) {
		switch (date('N', strtotime($res['time']))) {
			case '1':
				$Mon[$res['id']] = ['title' => $res['title'], 'time' => $res['time'], 'points' => $res['points']];
				break;
			case '2':
				$Tue[$res['id']] = ['title' => $res['title'], 'time' => $res['time'], 'points' => $res['points']];
				break;
			case '3':
				$Wed[$res['id']] = ['title' => $res['title'], 'time' => $res['time'], 'points' => $res['points']];
				break;
			case '4':
				$Thu[$res['id']] = ['title' => $res['title'], 'time' => $res['time'], 'points' => $res['points']];
				break;
			case '5':
				$Fri[$res['id']] = ['title' => $res['title'], 'time' => $res['time'], 'points' => $res['points']];
				break;
			case '6':
				$Sat[$res['id']] = ['title' => $res['title'], 'time' => $res['time'], 'points' => $res['points']];
				break;
			case '7':
				$Sun[$res['id']] = ['title' => $res['title'], 'time' => $res['time'], 'points' => $res['points']];
				break;
			default:
				break;
		}
	}

	// show task details
	function printDay($day, $dayArray) {
		foreach ($dayArray as $task) {
?>
			<tr>
				<td><?= $day ?></td>
				<td><?= $task['title'] ?></td>
				<td><?= $task['time'] ?></td>
				<td><?= $task['points'] ?></td>
			</tr>
			<?php
		}
	}

// Statistics by tasks
print('<div style="float:left;margin-left: 300px;"><h2 class="formBlock" style="padding-top: 0;">Statistics by tasks</h2>');
?>

<!-- task details table -->
<table>
    <tr>
        <th>Day</th>
        <th>Title</th>
        <th>Start time</th>
        <th>Points</th>
    </tr>
<?php
	if ($Mon) printDay('Monday', $Mon);
	if ($Tue) printDay('Tuesday', $Tue);
	if ($Wed) printDay('Wednesday', $Wed);
	if ($Thu) printDay('Thursday', $Thu);
	if ($Fri) printDay('Friday', $Fri);
	if ($Sat) printDay('Saturday', $Sat);
	if ($Sun) printDay('Sunday', $Sun);
	print('</table></div>');

	//
	//
	// daily detail section
	//
	//
	// Statistics by day
	print('<div style="float:right;margin-right: 300px;"><h2 class="formBlock" style="padding-top: 0;">Daily statistics</h2>');

	// show statistic by time (date)
	$get = $db->query("SELECT `time` FROM tasks WHERE username = '{$_COOKIE['user']}' ORDER BY time DESC; ");

	// show daily details
	$days = [];
	while ($res = $get->fetch_assoc()) {
		if (!in_array(date('md', strtotime($res['time'])), $days)) {
			$days[] = date('md', strtotime($res['time']));
		}
	}

	// show collected times in tasks for user
	foreach ($days as $current) {
		$m = substr($current, 0, 2);
		$d = substr($current, 2, 2);

		// get date details
		$get = $db->query("SELECT * FROM tasks WHERE username = '{$_COOKIE['user']}' AND MONTH(`time`) = '{$m}' AND DAY(`time`) = '{$d}' ; ");
		$c = $ic = $t = $tb = $cT = $icT = $tT = $tbT = 0;
		$hC = $mC = $hIn = $mIn = $mT = $hT = $hbT = $mbT = 0;

		while ($res = $get->fetch_assoc()) {
			$c += $res['completed'];
			$ic += $res['incompleted'];
			$t += $res['total'];
			$tb += $res['break'];
			$dd = $res['time'];

			// completed
			$cT += $res['completedTime'];
			$hC = intdiv($cT, 60);
			$mC = $cT % 60;

			// incompleted
			$tv = 0 + $res['incompletedTime'];
			if (is_float($tv)) {
				$tmp = explode('.', $tv);
				$min = intdiv($tmp[1], 60);
				$hIn += intdiv($tmp[0] + $min, 60);
				$mIn += ($tmp[0] + $min) % 60;
			}
			else {
				$hIn = intdiv($tv, 60);
				$mIn = $tv % 60;
			}

			// break
			$tbT += $res['breakTime'];
			$hbT = intdiv($tbT, 60);
			$mbT = $tbT % 60;

		}
		$cT = $hC . ':' . ($mC < 10 ? $mC.'0' : $mC);
		$icT = $hIn . ':' . ($mIn < 10 ? $mIn.'0' : $mIn);

		$mT = ($mC + $mIn) % 60;
		$hT = $hC + $hIn + intdiv($mC + $mIn, 60);
		$tT = $hT . ':' . ($mT < 10 ? $mT.'0' : $mT);

		$tbT = $hbT . ':' . ($mbT < 10 ? $mbT.'0' : $mbT);
?>
		<table>
			<tr>
				<th>Date</th>
				<th><?=date('F j', strtotime($dd)) ?></th>
				<th>hh:mm</th>
			</tr>
			<tr>
				<td>Completed</td>
				<td><?=$c ?></td>
				<td><?=$cT < 10 ? '0'.$cT : $cT?></td>
			</tr>
			<tr>
				<td>Incomplete</td>
				<td><?=$ic ?></td>
				<td><?=$icT < 10 ? '0'.$icT : $icT ?></td>
			</tr>
			<tr>
				<td>Total</td>
				<td><?=$t ?></td>
				<td><?=$tT < 10 ? '0'.$tT : $tT ?></td>
			</tr>
			<tr>
				<td>Break</td>
				<td><?=$tb ?></td>
				<td><?=$tbT < 10 ? '0'.$tbT : $tbT ?></td>
			</tr>
		</table>
		<br>

		<?php
	}

require_once 'footer.php';