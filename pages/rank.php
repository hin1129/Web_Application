<?php
    /*Connect to database*/
    require_once '../connection.php';
    if($db->connect_errno > 0){
        die('Unable to connect to database [' . $db->connect_error . ']');
    }

    //Page title
    $title = 'Top-5 users';
    require_once 'header.php';

    //Show users by points
    $get = $db->query("SELECT * FROM users ORDER BY points DESC LIMIT 5");
?>

<div class = "formBlock">
    <h1>Top 5 users</h1>

    <!-- Leaderboard table -->
    <table>
        <td>Username</td>
            <td>Points</td>
        </tr>
        <?php while ($res = $get->fetch_assoc()): ?>
            <tr>
                <td><?=$res['username']?></td>
                <td><?=$res['points'] ?></td>
            </tr>
        <?php endwhile; ?>
    </table>
</div>

<?php
    require_once 'footer.php';