<?php
	/*Connect to database*/
	require_once '../connection.php';
	if($db->connect_errno > 0){
		die('Unable to connect to database [' . $db->connect_error . ']');
	}

	//Page title
	$title = 'Achievements control';

	// database query
	if (isset($_POST['new'])) {
		//adding new achievements
		$db->query("INSERT INTO achievements (name, description) VALUES ('{$_POST['name']}', '{$_POST['desc']}');");
		header('Refresh: 0; url=achievements.php');
	}
	elseif (isset($_POST['assign'])) {
		//assign user with a specific achievement
		$get = $db->query("SELECT achievements FROM users WHERE username = '{$_POST['selectedUser']}';");
		$res = $get->fetch_assoc();
		$ach = $res['achievements'];
		$achArr = explode(' ', $res['achievements']);
		if (!in_array($_POST['selectedAch'], $achArr)) {
			$ach .= $_POST['selectedAch'];
			$ach .= ' ';
			$db->query("UPDATE users SET achievements = '{$ach}' WHERE username = '{$_POST['selectedUser']}';");
			header('Refresh: 1; url=achievements.php');
			print('Success assign, redirecting...');
		}
		else {
			header('Refresh: 1; url=achievements.php');
			print('Already assign, redirecting...');
		}
	}
	// html form
	else {
		require_once 'header.php';
		$get = $db->query("SELECT * FROM achievements;");
	}
?>

<!-- create achievements -->
<h3 style="text-align: center;">Create new achievement</h3>
<form method="post" class="formBlock" style="padding-top: 0px;">
	<input type="text" name="name" placeholder="Write achievements name"><br>
	<textarea name="desc" placeholder="Write achievements description"></textarea><br>
	<input type="submit" name="new" value="Create new">
</form>

<!-- list of achievements -->
<?php if ($get->num_rows != 0): ?>
<h2 style="text-align: center;">Achievements</h2>
<table>
	<tr>
		<th>Name</th>
		<th>Description</th>
	</tr>
	<?php while ($res = $get->fetch_assoc()): ?>
	<tr>
		<td><?=$res['name'] ?></td>
		<td><?=$res['description'] ?></td>
	</tr>
	<?php endwhile; ?>
</table>
	
<!-- assign button -->
<?php endif; ?>
<form method="post" class="formBlock" style="padding-top: 0px;">
	<h3>Select achievement</h3>
	<select name="selectedAch">
		<?php $get = $db->query("SELECT * FROM achievements;");

		while ($res = $get->fetch_assoc()): ?>
			<option value="<?=$res['id'] ?>"><?=$res['name'] ?></option>
		<?php endwhile; ?>
	</select><br>

	<h3>Select user</h3>
	<select name="selectedUser">
		<?php $get = $db->query("SELECT username FROM users;");
		
		while ($res = $get->fetch_assoc()): ?>
			<option value="<?=$res['username'] ?>"><?=$res['username'] ?></option>
		<?php endwhile; ?>
	</select><br><br>
	<input type="submit" name="assign" value="Assign to user">
</form>

<?php


require_once 'footer.php';