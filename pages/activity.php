<?php
    /*Connect to database*/
    require_once '../connection.php';
    if($db->connect_errno > 0){
        die('Unable to connect to database [' . $db->connect_error . ']');
    }

    //Page title
    $title = 'Activity page';

    if (isset($_POST['submit'])) {
        //add new task
        $db->query("INSERT INTO tasks (title, username) VALUES ('{$_POST['taskName']}', '{$_COOKIE['user']}');");
        $id = mysqli_insert_id($db);

        //Redirect to start timer
        SetCookie('title', "{$_POST['taskName']}", 0, '/');
        SetCookie('id', "{$id}", 0, '/');
        header('Refresh: 0; url=activity.php');
    }
    else if (isset($_POST['reset'])) {
        $p = (float)$_POST['points'];
        $c = (int)$_POST['completed'];
        $ic = (int)$_POST['incompleted'];
        $tb = (int)$_POST['taskBreak'];
        $t = $c + $ic;
        $totalTime = $_POST['completedTime'] + $_POST['incompletedTime'];
        $i = (int)$_COOKIE['id'];

        //adding collected points to user
        $db->query("UPDATE tasks SET points = {$p}, completed = {$c}, incompleted = {$ic}, total = {$t}, break = {$tb}, completedTime = {$_POST['completedTime']}, incompletedTime = {$_POST['incompletedTime']}, totalTime = {$totalTime}, breakTime = {$_POST['taskBreakTime']} WHERE id = {$i};");
        
        $res = $db->query("SELECT points FROM users WHERE username = '{$_COOKIE['user']}';");
        $get = $res->fetch_array();
        $total = $p + $get['points'];
        $db->query("UPDATE users SET points = {$total} WHERE username = '{$_COOKIE['user']}';");
        unset($_COOKIE['id']);
        unset($_COOKIE['title']);
        SetCookie('id', null, -1, '/');
        SetCookie('title', null, -1, '/');
        header('Refresh: 0; url=activity.php');
    }
    
    else {
        require_once 'header.php';
        if ($_COOKIE['id']) {
?>
            <h2>Now you are working on: <?=$_COOKIE['title'] ?> (MUST PRESS RESET TO COLLECT POINTS)</h2>

            <div class="timer">
                <span id="minutes"></span>
                <span id="delimiter">:</span>
                <span id="seconds"></span>
            </div>

            <form method="post">
                <input type="button" name="stop" value="Stop" onclick="stopButton();" id="stop">
                <input type="button" name="reset" value="Reset" onclick="resetButton();">
            </form>
            <?php
        }
        else {
            ?>
            <form method="post" class="formBlock">
                <p>Enter a title for the task: </p>
                <input type="text" name="taskName"><br>
                <input type="submit" value="Create Task and Starts" name="submit">
            </form>
            <?php
        }
    }

require_once 'footer.php';