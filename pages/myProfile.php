<?php
    /*Connect to database*/
    require_once '../connection.php';
    if($db->connect_errno > 0){
        die('Unable to connect to database [' . $db->connect_error . ']');
    }

    //Page title
    $title = 'My Profile';

    require_once 'header.php';
    //Show user profile
    $get = $db->query("SELECT * FROM users WHERE username = '{$_COOKIE['user']}'");
    $res = $get->fetch_assoc();
?>

<!-- account detail table -->
<table>
	<tr>
		<td>
			<img src="<?=$res['picture'] ?>" alt="picture" width="300px" height="200px">
		</td>
		<td>
			<h1>Your account details</h1>
			<p>Your username: <?=$res['username'] ?></p>
			<p>Your email: <?=$res['email'] ?></p>
			<p>Current password: <?=$res['password'] ?></p>
			<p>Collected points: <?=$res['points'] ?></p>
		</td>
	</tr>
</table>

<!-- achievements table -->
<?php if($res['achievements']) {?>
	<!-- Show users achievements -->
	<h3 style="text-align: center;">Your Achievements</h3>
	<table>
		<tr>
			<th>Name</th>
			<th>Description</th>
		</tr>
		<?php
		$get = $db->query("SELECT achievements FROM users WHERE username = '{$res['username']}';");
		$res = $get->fetch_assoc();

		$achArr = explode(' ', $res['achievements']);
		for ($i = 0; $i < (count($achArr) - 1); $i++) {
			$get = $db->query("SELECT * FROM achievements WHERE id = {$achArr[$i]};");
			$res = $get->fetch_assoc();
		?>
		<tr>
			<td><?=$res['name'] ?></td>
			<td><?=$res['description'] ?></td>
		</tr>
		<?php
	}
	print('</table>');
}
		?>

<?php
require_once 'footer.php';