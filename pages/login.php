<?php
    /*Connect to database*/
    require_once '../connection.php';
    if($db->connect_errno > 0){
        die('Unable to connect to database [' . $db->connect_error . ']');
    }

    //Page title
    $title = 'Sign In';

    if (isset($_POST['submit'])) {
        // Check for correct user input
        $get = $db->query("SELECT username FROM users WHERE username = '{$_POST['login']}' AND password = '{$_POST['password']}'");
        if ($get->fetch_assoc() != null) {
            //redirect if login successful
            SetCookie('user', "{$_POST['login']}", 0, '/');
            header( 'Refresh: 3; url=../index.php' );
            print 'Successfully logged in, you will be redirected in 3 seconds.';
        }
        else {
            // wrong login
           header('Refresh: 4; url=login.php');
           print 'Wrong username or password, please write them correctly.';
        }
    }
    else {
        require_once 'header.php';
?>
        <div class = "formBlock">
           <h1>Sign In</h1>
            <form method="post">
                <input type="text" name="login" placeholder="Write your username"><br>
                <input type="password" name="password" placeholder="Write password"><br>
                <input type="submit" name="submit" value="login">
            </form>
        </div>
        <?php
    }

require_once 'footer.php';