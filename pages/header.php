<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Responsive meta tag (from bootstrap) -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

    <meta charset="UTF-8">
    <title><?=$title?></title>
    <link rel="stylesheet" href="../css/style.css">
    <script src="../js/script.js" type="text/javascript"></script>
</head>

<body>
    <header>
        <ul class="float" id="menu">
            <li><a href="../index.php">Home</a></li>
            <li><a href="rank.php">Leaderboard</a></li>
            <?php
                if (isset($_COOKIE['user'])) {
                    // admin
                    if ($_COOKIE['user'] == 'admin') {
                        print '<li><a href="achievements.php">Achievements</a></li>';
                    }

                    // non-admin
                    print '<li><a href="myProfile.php">My Profile</a></li>';
                    print '<li><a href="activity.php">Activity</a></li>';
                    print '<li><a href="statistic.php">Statistical</a></li>';
                    print '<li><a href="comments.php">Comments</a></li>';
                    print '<li><a href="logout.php">Sign out</a></li>';
                }
                else {
                    print '<li><a href="login.php">Sign In</a></li>';
                    print '<li><a href="register.php">Sign Up</a></li>';
                }
            ?>
        </ul>
    </header>
    <div class="clear"></div>
</body>