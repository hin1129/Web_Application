function updater(m, s, A) {
  var baseTime = new Date();
  var period = A*60*1000;

  // Function to set timer
  function update() {
    var cur = new Date();
    var diff = period - (cur - baseTime) % period;
    var millis = diff % 1000;
    diff = Math.floor(diff/1000);
    var sec = diff % 60;
    if(sec < 10) sec = "0"+sec;
    diff = Math.floor(diff/60);
    var min = diff % 60;
    if(min < 10) min = "0"+min;
    m.innerHTML = min;
    s.innerHTML = sec;

    // incompleted tasks
    if(min !== "00" || sec !== "00"){
      if (isStop === 1) {
        isStop = 0;
        if(parseInt(min) > 10) mnoj = 0;
        else mnoj = 0.5;
        flag = 1;
        alert("Time to break!");
        document.getElementById("stop").setAttribute("disabled", "disabled");
        document.getElementsByTagName("h2")[0].innerHTML = "Break";
        incompleted++;
        incompletedTime += parseInt(min) + parseInt(sec) / 100;
        updater(document.getElementById("minutes"), document.getElementById("seconds"), 5); //5 min = 5
      }
      else setTimeout(update, millis);
    }
    // completed tasks
    else {
      if(flag === 0) {
        flag = 1;
        alert("Time to break!");
        document.getElementById("stop").setAttribute("disabled", "disabled");
        document.getElementsByTagName("h2")[0].innerHTML = "Break";
        completed++;
        completedTime += 25;
        updater(document.getElementById("minutes"), document.getElementById("seconds"), 5); //5 min = 5
      }
      else if(flag === 1){
        flag = 0;
        points += mnoj;
        mnoj = 1;
        alert("It's time to work!");
        document.getElementById("stop").removeAttribute("disabled");
        document.getElementsByTagName("h2")[0].innerHTML = "Now you are working on: " + getCookie("title");
        taskBreak++;
        taskBreakTime += 5;
        updater(document.getElementById("minutes"), document.getElementById("seconds"), 25); //25 min = 25
      }
    }
  }
  setTimeout(update, 0);
}

/*Reset button function*/
function resetButton() {
  submit('../pages/activity.php', {
    reset: 'Ok',
    points: points,
    completed: completed,
    incompleted: incompleted,
    taskBreak: taskBreak,
    completedTime: completedTime,
    incompletedTime: incompletedTime,
    taskBreakTime: taskBreakTime
  });
}

// Stop button function
function stopButton() {
  if(confirm("Are you sure you want to stop?")){
   isStop = 1;      
  }
}

var flag = 0;
var points = 0;
var isStop = 0;
var mnoj = 1; //point the user gets from task
var completed = 0;
var completedTime = 0;
var incompleted = 0;
var incompletedTime = 0;
var taskBreak = 0;
var taskBreakTime = 0;

// Start function
window.onload = function(){
  updater(document.getElementById("minutes"), document.getElementById("seconds"), 25);	//25 min = 25
}

function buildElement(tagName, props) {
  var element = document.createElement(tagName);
  for (var propName in props) element[propName] = props[propName];
  return element;
}

// Hidden post for buttons
function submit(link, props) {
  var form = buildElement('form', {
    method: 'post',
    action: link
  });
  for (var propName in props) form.appendChild(
    buildElement('input', {
      type: 'hidden',
      name: propName,
      value: props[propName]
    })
  );
  form.style.display = 'none';
  document.body.appendChild(form);
  form.submit();
}

// Cookie function
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}