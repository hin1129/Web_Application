-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 21, 2018 at 08:03 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `app`
--

-- --------------------------------------------------------

--
-- Table structure for table `achievements`
--

CREATE TABLE `achievements` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `achievements`
--

INSERT INTO `achievements` (`id`, `name`, `description`) VALUES
(1, 'Student-like', 'You have been logged for 5 days'),
(2, 'Government-like', 'You have been visiting for 21 days'),
(3, 'Slave-like', 'You have been visiting for 28 days'),
(4, 'Leaver', 'Quit during the task 2 times'),
(5, 'Trash', 'Quit during the task 5 times'),
(6, 'Brain dead', 'Quit during the task 7 times'),
(7, 'Newbie', 'Complete tasks 1st time'),
(8, 'Expert', 'Complete tasks 3 times'),
(9, 'Veteran', 'Complete tasks 10 times'),
(10, 'Top of the world', 'Top scores on the leaderboard for 3 days'),
(11, 'Top of the space', 'Top scores on the leaderboard for 10 days'),
(12, 'Top of the galaxy', 'Top scores on the leaderboard for 30 days'),
(13, 'Ametaur', 'Top at most consecutive logged in on the leaderboard for 3 days'),
(14, 'Professional', 'Top at most consecutive logged in on the leaderboard for 10 days'),
(15, 'Masterclass', 'Top at most consecutive logged in on the leaderboard for 30 days'),
(16, 'Day dreamer', 'Logged in but not performing task for 1 hour'),
(17, 'Boss-like', 'Logged in but not performing task for 3 hours'),
(18, 'Jeff Bezos-like', 'Logged in but not performing task for 6 hours');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(30) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `username`, `date`, `title`, `description`) VALUES
(1, 'user6', '2018-03-06 11:20:57', 'comment1', 'Hi world'),
(2, 'user3', '2018-03-06 11:22:12', 'comment2', 'Another user');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `points` float NOT NULL DEFAULT '0',
  `completed` int(11) NOT NULL DEFAULT '0',
  `incompleted` int(11) NOT NULL DEFAULT '0',
  `total` int(11) NOT NULL DEFAULT '0',
  `break` int(11) NOT NULL DEFAULT '0',
  `completedTime` int(11) NOT NULL,
  `incompletedTime` float NOT NULL,
  `totalTime` float NOT NULL,
  `breakTime` int(11) NOT NULL,
  `username` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `title`, `time`, `points`, `completed`, `incompleted`, `total`, `break`, `completedTime`, `incompletedTime`, `totalTime`, `breakTime`, `username`) VALUES
(1, 'task1', '2018-03-06 11:15:47', 0, 1, 0, 0, 0, 0, 0, 0, 0, 'user6'),
(2, 'task2', '2018-03-06 11:19:47', 2, 1, 0, 0, 0, 0, 0, 0, 0, 'user6'),
(3, 'task1', '2018-03-15 05:21:40', 0, 1, 0, 0, 0, 0, 0, 0, 0, 'user1'),
(4, 'test1', '2018-03-15 05:24:35', 2, 1, 0, 0, 0, 0, 0, 0, 0, 'user1'),
(5, 'test2', '2018-03-15 05:26:34', 2, 1, 0, 0, 0, 0, 0, 0, 0, 'user1'),
(6, 'test_2', '2018-03-18 19:25:54', 3, 4, 0, 4, 3, 0, 0, 0, 0, 'user1'),
(7, 'test_3', '2018-03-18 19:28:46', 0.5, 1, 1, 2, 1, 0, 0, 0, 0, 'user1'),
(8, 'test_4', '2018-03-18 20:38:55', 2, 2, 0, 2, 2, 0, 0, 0, 0, 'user1'),
(10, 'task_6', '2018-03-19 07:27:44', 0, 0, 0, 0, 0, 0, 0, 0, 0, 'user1'),
(11, 'tasq', '2018-03-19 07:28:55', 0, 0, 0, 0, 0, 0, 0, 0, 0, 'user1'),
(12, 'tass', '2018-03-19 07:35:50', 2, 2, 1, 3, 2, 0, 0, 0, 0, 'user1'),
(13, 'testing', '2018-03-19 07:42:56', 0, 1, 0, 1, 0, 0, 0, 0, 0, 'user1'),
(14, 'qwer', '2018-03-19 07:43:46', 0, 0, 0, 0, 0, 0, 0, 0, 0, 'user1'),
(15, 'time', '2018-03-19 08:00:55', 0, 0, 0, 0, 0, 0, 0, 0, 0, 'user1'),
(16, 'k', '2018-03-19 08:22:14', 0, 0, 0, 0, 0, 0, 0, 0, 0, 'user1'),
(17, 'kk', '2018-03-19 08:23:49', 2, 2, 0, 2, 2, 50, 0, 50, 10, 'user1'),
(18, 'gg', '2018-03-19 08:25:34', 1.5, 1, 1, 2, 2, 25, 0, 25, 10, 'user1'),
(19, 'gg', '2018-03-19 16:04:06', 2, 1, 2, 3, 3, 25, 0, 25, 15, 'user1'),
(20, 'ff', '2018-03-19 16:06:31', 2, 1, 2, 3, 3, 25, 0, 25, 15, 'user1'),
(21, 'final', '2018-03-19 16:10:32', 0.5, 0, 1, 1, 1, 0, 0.01, 0, 5, 'user1'),
(22, 'tre', '2018-03-19 16:13:34', 0.5, 0, 1, 1, 1, 0, 0.02, 0, 5, 'user1'),
(23, 'q', '2018-03-19 16:17:32', 0, 0, 1, 1, 0, 0, 3, 3, 0, 'user1'),
(24, 'qw', '2018-03-19 16:22:32', 1, 1, 3, 4, 2, 25, 0.11, 25.11, 10, 'user1'),
(25, 'Go', '2018-03-19 17:34:10', 0, 0, 0, 0, 0, 0, 0, 0, 0, 'user1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `picture` varchar(60) NOT NULL,
  `points` float NOT NULL DEFAULT '0',
  `achievements` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`, `email`, `picture`, `points`, `achievements`) VALUES
('admin', 'admin', 'email@email.com', '../images/12.jpg', 0, '1 2 '),
('user1', '1', 'email@email.com', '../images/1.jpg', 35, NULL),
('user2', '1', 'email@email.com', '../images/2.jpg', 3, NULL),
('user3', '1', 'email@email.com', '../images/3.jpg', 7, NULL),
('user4', '1', 'email@email.com', '../images/4.jpg', 4, NULL),
('user5', '1', 'email@email.com', '../images/5.jpg', 0, NULL),
('user6', '1', 'email@email.com', '../images/6.jpg', 6, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `achievements`
--
ALTER TABLE `achievements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `achievements`
--
ALTER TABLE `achievements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;